# Example file for upstream/metadata.
# See https://wiki.debian.org/UpstreamMetadata for more info/fields.
# Below an example based on a github project.

# Bug-Database: https://github.com/<user>/coil64/issues
# Bug-Submit: https://github.com/<user>/coil64/issues/new
# Changelog: https://github.com/<user>/coil64/blob/master/CHANGES
# Documentation: https://github.com/<user>/coil64/wiki
# Repository-Browse: https://github.com/<user>/coil64
# Repository: https://github.com/<user>/coil64.git
